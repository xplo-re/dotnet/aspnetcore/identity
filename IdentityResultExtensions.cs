﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Identity;


namespace XploRe.AspNetCore.Identity
{

    /// <summary>
    ///     Extends <see cref="IdentityResult" /> objects with convenience methods to get error codes, descriptions and 
    ///     a synthesised error message.
    /// </summary>
    public static class IdentityResultExtensions
    {

        /// <summary>
        ///     Retrieves all error codes from the <see cref="IdentityResult" /> as a list.
        /// </summary>
        /// <param name="result">The <see cref="IdentityResult" /> instance to extract error codes from.</param>
        /// <returns>List of all non-empty error codes from the provided <paramref name="result" />.</returns>
        [Pure]
        [NotNull]
        [ItemNotNull]
        public static IList<string> ErrorCodes([NotNull] this IdentityResult result)
        {
            if (result == null) {
                throw new ArgumentNullException(nameof(result));
            }

            return result.Errors?.Select(error => error?.Code)
                         .Where(text => !string.IsNullOrEmpty(text))
                         .ToList()
                   ?? new List<string>(0);
        }

        /// <summary>
        ///     Retrieves all error descriptions from the <see cref="IdentityResult" /> as a list.
        /// </summary>
        /// <param name="result">The <see cref="IdentityResult" /> instance to extract error descriptions from.</param>
        /// <returns>List of all non-empty error descriptions from the provided <paramref name="result" />.</returns>
        [Pure]
        [NotNull]
        [ItemNotNull]
        public static IList<string> ErrorDescriptions([NotNull] this IdentityResult result)
        {
            if (result == null) {
                throw new ArgumentNullException(nameof(result));
            }

            return result.Errors?.Select(error => error?.Description)
                         .Where(text => !string.IsNullOrEmpty(text))
                         .ToList()
                   ?? new List<string>(0);
        }

        /// <summary>
        ///     Composes an error message from all error descriptions in the <see cref="IdentityResult" />.
        /// </summary>
        /// <param name="result">The <see cref="IdentityResult" /> instance to extract error descriptions from.</param>
        /// <returns>
        ///     Error message derived from error descriptions in <paramref name="result" />. If no error descriptions
        ///     are found, the returned string is empty.
        /// </returns>
        [Pure]
        [NotNull]
        public static string ErrorMessage([NotNull] this IdentityResult result)
        {
            return string.Join(", ", ErrorDescriptions(result)) ?? string.Empty;
        }

    }

}
